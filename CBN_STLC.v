Require Import Utf8.

Inductive inc (V : Set) : Set :=
| VZ : inc V
| VS : V → inc V
.

Arguments VZ {V}.
Arguments VS {V}.

Definition inc_map {A B : Set} (f : A → B) (x : inc A) : inc B :=
  match x with
  | VZ   => VZ
  | VS y => VS (f y)
  end.

Inductive term (V : Set) : Set :=
| tm_var : V → term V
| tm_lam : term (inc V) → term V
| tm_app : term V → term V → term V
.

Arguments tm_var [V] _.
Arguments tm_lam [V] _.
Arguments tm_app [V] _ _.

Notation term0  := (term Empty_set).

Fixpoint map {A B : Set} (f : A → B) (t : term A) : term B :=
  match t with
  | tm_var x     => tm_var (f x)
  | tm_lam t     => tm_lam (map (inc_map f) t)
  | tm_app t₁ t₂ => tm_app (map f t₁) (map f t₂)
  end.

Definition lift {A B : Set} (f : A → term B) (x : inc A) : term (inc B) :=
  match x with
  | VZ   => tm_var VZ
  | VS y => map VS (f y)
  end.

Fixpoint bind {A B : Set} (f : A → term B) (t : term A) : term B :=
  match t with
  | tm_var x     => f x
  | tm_lam t     => tm_lam (bind (lift f) t)
  | tm_app t₁ t₂ => tm_app (bind f t₁) (bind f t₂)
  end.

Definition subst_func {V : Set} (s : term V) (x : inc V) : term V :=
  match x with
  | VZ   => s
  | VS y => tm_var y
  end.

Notation subst t s := (bind (subst_func s) t).

Inductive red {V : Set} : term V → term V → Prop :=
| red_beta : ∀ t s,
    red (tm_app (tm_lam t) s) (subst t s)
| red_app : ∀ t t' s,
    red t t' →
    red (tm_app t s) (tm_app t' s)
.

Inductive tp : Set :=
| tp_base  : tp
| tp_arrow : tp → tp → tp
.

Definition env (V : Set) : Set := V → tp.

Definition env_empty (x : Empty_set) : tp :=
  match x with end.

Definition env_ext {V : Set} (Γ : env V) (τ : tp) (x : inc V) : tp :=
  match x with
  | VZ   => τ
  | VS y => Γ y
  end.

Notation "∅" := (env_empty).
Notation "Γ ',+' τ" := (env_ext Γ τ) (at level 45, left associativity).

Reserved Notation "'T[' Γ '⊢' t '⇒' τ ']'".

Inductive typing {V : Set} (Γ : env V) : term V → tp → Prop :=
| T_var : ∀ x,
    T[ Γ ⊢ tm_var x ⇒ Γ x ]
| T_lam : ∀ t σ τ,
    T[ Γ ,+ σ ⊢ t ⇒ τ ] →
    T[ Γ ⊢ tm_lam t ⇒ tp_arrow σ τ ]
| T_app : ∀ t s σ τ,
    T[ Γ ⊢ t ⇒ tp_arrow σ τ ] →
    T[ Γ ⊢ s ⇒ σ ] →
    T[ Γ ⊢ tm_app t s ⇒ τ ]
where "T[ Γ ⊢ t ⇒ τ ]" := (@typing _ Γ t τ).

Lemma progress_aux {V : Set} (Γ : env V) t τ : T[ Γ ⊢ t ⇒ τ ] →
  V = Empty_set →
  (∃ t', t = tm_lam t') ∨ (∃ t', red t t').
Proof.
induction 1; intro; subst.
+ destruct x.
+ eauto.
+ right; destruct IHtyping1 as [[]|[]]; trivial.
  - subst; eexists; constructor 1.
  - eexists; constructor 2; eassumption.
Qed.

Theorem progress (t : term0) τ : T[ ∅ ⊢ t ⇒ τ ] →
  (∃ t', t = tm_lam t') ∨ (∃ t', red t t').
Proof.
intros; eapply progress_aux; eauto.
Qed.

Lemma typing_map {A B : Set}
  (Γ : env A) (Δ : env B)
  (f : A → B) t τ :
  (∀ x, Δ (f x) = Γ x) →
  T[ Γ ⊢ t ⇒ τ ] →
  T[ Δ ⊢ map f t ⇒ τ ].
Proof.
intros Hf Htp; generalize B Δ f Hf; clear B Δ f Hf;
  induction Htp; simpl; intros B Δ f Hf.
+ rewrite <- Hf; constructor.
+ constructor; apply IHHtp.
  intros [ | x ]; simpl; auto.
+ econstructor; eauto.
Qed.

Lemma typing_bind {A B : Set}
  (Γ : env A) (Δ : env B)
  (f : A → term B) t τ :
  (∀ x, T[ Δ ⊢ f x ⇒ Γ x ]) →
  T[ Γ ⊢ t ⇒ τ ] →
  T[ Δ ⊢ bind f t ⇒ τ ].
Proof.
intros Hf Htp; generalize B Δ f Hf; clear B Δ f Hf;
  induction Htp; simpl; intros B Δ f Hf.
+ auto.
+ constructor; apply IHHtp.
  intros [ | x ]; simpl; [ constructor | ].
  eapply typing_map; [ | apply Hf ]; reflexivity.
+ econstructor; eauto.
Qed.

Theorem subject_reduction (t t' : term0) τ :
  red t t' → T[ ∅ ⊢ t ⇒ τ ] → T[ ∅ ⊢ t' ⇒ τ ].
Proof.
intro Hred; generalize τ; clear τ.
induction Hred; intros τ Htp; inversion Htp.
+ inversion H1.
  eapply typing_bind; [ | eassumption ].
  intros [ | [] ]; simpl; assumption.
+ econstructor; eauto.
Qed.

(* ========================================================================= *)
(* Metatheory -- not needed in the proof *)

Lemma monad_map_id {A : Set} (f : A → A) t :
  (∀ x, f x = x) → map f t = t.
Proof.
induction t; simpl; intro Hf.
+ rewrite Hf; reflexivity.
+ rewrite IHt; [ reflexivity | ].
  intros [ | x ]; simpl; [ | rewrite Hf ]; reflexivity.
+ rewrite IHt1, IHt2; auto.
Qed.

Lemma monad_map_map {A B C : Set} (f₁ : B → C) (f₂ : A → B) f t :
  (∀ x, f₁ (f₂ x) = f x) → map f₁ (map f₂ t) = map f t.
Proof.
generalize B C f f₁ f₂; clear B C f f₁ f₂.
induction t; simpl; intros B C f f₁ f₂ Hf.
+ rewrite Hf; reflexivity.
+ erewrite IHt; [ reflexivity | ].
  intros [ | x ]; simpl; [ | rewrite Hf ]; reflexivity.
+ erewrite IHt1, IHt2; auto.
Qed.

Lemma monad_map_map' {A B C : Set} (f₁ : B → C) (f₂ : A → B) t :
  map f₁ (map f₂ t) = map (λ x, f₁ (f₂ x)) t.
Proof.
apply monad_map_map; reflexivity.
Qed.

Lemma monad_bind_return {A : Set} (f : A → term A) t :
  (∀ x, f x = tm_var x) → bind f t = t.
Proof.
induction t; simpl; intro Hf.
+ auto.
+ rewrite IHt; [ reflexivity | ].
  intros [ | x ]; simpl; [ | rewrite Hf ]; reflexivity.
+ erewrite IHt1, IHt2; auto.
Qed.

Lemma monad_bind_map {A B B' C : Set}
  (f₁ : B → term C) (f₂ : A → B)
  (g₁ : B' → C) (g₂ : A → term B') t :
  (∀ x, f₁ (f₂ x) = map g₁ (g₂ x)) →
  bind f₁ (map f₂ t) = map g₁ (bind g₂ t).
Proof.
generalize B B' C f₁ f₂ g₁ g₂; clear B B' C f₁ f₂ g₁ g₂.
induction t; simpl; intros B B' C f₁ f₂ g₁ g₂ Hfg.
+ apply Hfg.
+ erewrite IHt; [ reflexivity | ].
  intros [ | x ]; simpl; [ reflexivity | rewrite Hfg ].
  rewrite monad_map_map'; symmetry; apply monad_map_map; reflexivity.
+ erewrite IHt1, IHt2; auto.
Qed.

Lemma monad_bind_bind {A B C : Set}
  (f₁ : B → term C) (f₂ : A → term B) (g : A → term C) t :
  (∀ x, bind f₁ (f₂ x) = g x) →
  bind f₁ (bind f₂ t) = bind g t.
Proof.
generalize B C f₁ f₂ g; clear B C f₁ f₂ g.
induction t; simpl; intros B C f₁ f₂ g Hfg.
+ apply Hfg.
+ erewrite IHt; [ reflexivity | ].
  intros [ | x ]; simpl; [ reflexivity | rewrite <- Hfg ].
  apply monad_bind_map; reflexivity.
+ erewrite IHt1, IHt2; auto.
Qed.