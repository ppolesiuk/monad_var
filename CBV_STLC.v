Require Import Utf8.

Inductive inc (V : Set) : Set :=
| VZ : inc V
| VS : V → inc V
.

Arguments VZ [V].
Arguments VS [V] _.

Definition inc_map {A B : Set} (f : A → B) (x : inc A) : inc B :=
  match x with
  | VZ   => VZ
  | VS y => VS (f y)
  end.

Inductive term (V : Set) : Set :=
| tm_value : value V → term V
| tm_app   : term V → term V → term V
with value (V : Set) : Set :=
| val_var : V → value V
| val_lam : term (inc V) → value V
.

Arguments tm_value [V] _.
Arguments tm_app   [V] _ _.
Arguments val_var  [V] _.
Arguments val_lam  [V] _.

Coercion tm_value : value >-> term.

Notation term0  := (term Empty_set).
Notation value0 := (value Empty_set).

Fixpoint tmap {A B : Set} (f : A → B) (t : term A) : term B :=
  match t with
  | tm_value v => vmap f v
  | tm_app t s => tm_app (tmap f t) (tmap f s)
  end
with vmap {A B : Set} (f : A → B) (v : value A) : value B :=
  match v with
  | val_var x => val_var (f x)
  | val_lam t => val_lam (tmap (inc_map f) t)
  end.

Notation tshift := (tmap (@VS _)).
Notation vshift := (vmap (@VS _)).

Fixpoint tmonad_map_id {A : Set} (f : A → A) t :
  (∀ x, f x = x) → tmap f t = t
with vmonad_map_id {A : Set} (f : A → A) v :
  (∀ x, f x = x) → vmap f v = v.
Proof.
+ intro Hf; destruct t as [ v | t s ]; simpl.
  - rewrite vmonad_map_id; auto.
  - rewrite tmonad_map_id, tmonad_map_id; auto.
+ intro Hf; destruct v as [ x | t ]; simpl.
  - rewrite Hf; reflexivity.
  - rewrite tmonad_map_id; [ reflexivity | ].
    intros [ | x ]; simpl; [ reflexivity | ].
    rewrite Hf; reflexivity.
Qed.

Lemma tmonad_map_id' {A : Set} (t : term A) :
  tmap (λ x, x) t = t.
Proof.
apply tmonad_map_id; reflexivity.
Qed.

Lemma vmonad_map_id' {A : Set} (v : value A) :
  vmap (λ x, x) v = v.
Proof.
apply vmonad_map_id; reflexivity.
Qed.

Fixpoint tmonad_map_map {A B C : Set} (f₁ : B → C) (f₂ : A → B) f t :
  (∀ x, f₁ (f₂ x) = f x) → tmap f₁ (tmap f₂ t) = tmap f t
with vmonad_map_map {A B C : Set} (f₁ : B → C) (f₂ : A → B) f v :
  (∀ x, f₁ (f₂ x) = f x) → vmap f₁ (vmap f₂ v) = vmap f v.
Proof.
+ intro Hf; destruct t as [ v | t s ]; simpl.
  - erewrite vmonad_map_map; [ reflexivity | ].
    assumption.
  - erewrite tmonad_map_map; [ | eassumption ].
    erewrite tmonad_map_map; [ | eassumption ].
    reflexivity.
+ intro Hf; destruct v as [ x | t ]; simpl.
  - rewrite Hf; reflexivity.
  - erewrite tmonad_map_map; [ reflexivity | ].
    intros [ | x ]; simpl; [ reflexivity | ].
    rewrite Hf; reflexivity.
Qed.

Lemma tmonad_map_map' {A B C : Set} (f₁ : B → C) (f₂ : A → B) t :
  tmap f₁ (tmap f₂ t) = tmap (λ x, f₁ (f₂ x)) t.
Proof.
apply tmonad_map_map; reflexivity.
Qed.

Lemma vmonad_map_map' {A B C : Set} (f₁ : B → C) (f₂ : A → B) v :
  vmap f₁ (vmap f₂ v) = vmap (λ x, f₁ (f₂ x)) v.
Proof.
apply vmonad_map_map; reflexivity.
Qed.

Definition lift {A B : Set} (f : A → value B) (x : inc A) : value (inc B) :=
  match x with
  | VZ   => val_var VZ
  | VS y => vshift (f y)
  end.

Fixpoint tbind {A B : Set} (f : A → value B) (t : term A) : term B :=
  match t with
  | tm_value v => vbind f v
  | tm_app t s => tm_app (tbind f t) (tbind f s)
  end
with vbind {A B : Set} (f : A → value B) (v : value A) : value B :=
  match v with
  | val_var x => f x
  | val_lam t => val_lam (tbind (lift f) t)
  end.

Fixpoint tmonad_bind_return {A : Set} (f : A → value A) t :
  (∀ x, f x = val_var x) → tbind f t = t
with vmonad_bind_return {A : Set} (f : A → value A) v :
  (∀ x, f x = val_var x) → vbind f v = v.
Proof.
+ intro Hf; destruct t as [ v | t s ]; simpl.
  - erewrite vmonad_bind_return; [ reflexivity | ].
    assumption.
  - erewrite tmonad_bind_return; [ | eassumption ].
    erewrite tmonad_bind_return; [ | eassumption ].
    reflexivity.
+ intro Hf; destruct v as [ x | t ]; simpl.
  - apply Hf.
  - rewrite tmonad_bind_return; [ reflexivity | ].
    intros [ | x ]; simpl; [ reflexivity | ].
    rewrite Hf; reflexivity.
Qed.

Lemma tmonad_bind_return' {A : Set} (t : term A) :
  tbind (@val_var _) t = t.
Proof.
apply tmonad_bind_return; reflexivity.
Qed.

Lemma vmonad_bind_return' {A : Set} (v : value A) :
  vbind (@val_var _) v = v.
Proof.
apply vmonad_bind_return; reflexivity.
Qed.

Fixpoint tmonad_bind_map {A B B' C : Set}
  (f₁ : B → value C) (f₂ : A → B)
  (g₁ : B' → C) (g₂ : A → value B') t :
  (∀ x, f₁ (f₂ x) = vmap g₁ (g₂ x)) →
  tbind f₁ (tmap f₂ t) = tmap g₁ (tbind g₂ t)
with vmonad_bind_map {A B B' C : Set}
  (f₁ : B → value C) (f₂ : A → B)
  (g₁ : B' → C) (g₂ : A → value B') v :
  (∀ x, f₁ (f₂ x) = vmap g₁ (g₂ x)) →
  vbind f₁ (vmap f₂ v) = vmap g₁ (vbind g₂ v).
Proof.
+ intro Hf; destruct t as [ v | t s ]; simpl.
  - erewrite vmonad_bind_map; [ reflexivity | ].
    assumption.
  - erewrite tmonad_bind_map; [ | eassumption ].
    erewrite tmonad_bind_map; [ | eassumption ].
    reflexivity.
+ intro Hf; destruct v as [ x | t ]; simpl.
  - apply Hf.
  - erewrite tmonad_bind_map; [ reflexivity | ].
    intros [ | x ]; simpl; [ reflexivity | ].
    rewrite Hf, vmonad_map_map', vmonad_map_map'; simpl; reflexivity.
Qed.

Fixpoint tmonad_bind_bind {A B C : Set}
  (f₁ : B → value C) (f₂ : A → value B) (g : A → value C) t :
  (∀ x, vbind f₁ (f₂ x) = g x) →
  tbind f₁ (tbind f₂ t) = tbind g t
with vmonad_bind_bind {A B C : Set}
  (f₁ : B → value C) (f₂ : A → value B) (g : A → value C) v :
  (∀ x, vbind f₁ (f₂ x) = g x) →
  vbind f₁ (vbind f₂ v) = vbind g v.
Proof.
+ intro Hf; destruct t as [ v | t s ]; simpl.
  - erewrite vmonad_bind_bind; [ reflexivity | ].
    assumption.
  - erewrite tmonad_bind_bind; [ | eassumption ].
    erewrite tmonad_bind_bind; [ | eassumption ].
    reflexivity.
+ intro Hf; destruct v as [ x | t ]; simpl.
  - apply Hf.
  - erewrite tmonad_bind_bind; [ reflexivity | ].
    intros [ | x ]; simpl; [ reflexivity | ].
    rewrite <- Hf. apply vmonad_bind_map. reflexivity.
Qed.

Lemma tmonad_bind_bind' {A B C : Set}
  (f₁ : B → value C) (f₂ : A → value B) t :
  tbind f₁ (tbind f₂ t) = tbind (λ x, vbind f₁ (f₂ x)) t.
Proof.
apply tmonad_bind_bind; reflexivity.
Qed.

Lemma vmonad_bind_bind' {A B C : Set}
  (f₁ : B → value C) (f₂ : A → value B) v :
  vbind f₁ (vbind f₂ v) = vbind (λ x, vbind f₁ (f₂ x)) v.
Proof.
apply vmonad_bind_bind; reflexivity.
Qed.

Definition subst_func {V : Set} (v : value V) (x : inc V) : value V :=
  match x with
  | VZ   => v
  | VS y => val_var y
  end.

Notation tsubst t v := (tbind (subst_func v) t).
Notation vsubst w v := (vbind (subst_func v) w).

Inductive red {V : Set} : term V → term V → Prop :=
| red_beta : ∀ t (v : value V),
    red (tm_app (val_lam t) v) (tsubst t v)
| red_app1 : ∀ t t' s,
    red t t' →
    red (tm_app t s) (tm_app t' s)
| red_app2 : ∀ (v : value V) t t',
    red t t' →
    red (tm_app v t) (tm_app v t')
.

Inductive tp : Set :=
| tp_base  : tp
| tp_arrow : tp → tp → tp
.

Definition env (V : Set) : Set := V → tp.

Definition env_empty (x : Empty_set) : tp :=
  match x with end.

Definition env_ext {V : Set} (Γ : env V) (τ : tp) (x : inc V) : tp :=
  match x with
  | VZ   => τ
  | VS y => Γ y
  end.

Notation "∅" := (env_empty).
Notation "Γ ',+' τ" := (env_ext Γ τ) (at level 45, left associativity).

Reserved Notation "'T[' Γ '⊢' t ':→' τ ']'".

Inductive typing {V : Set} (Γ : env V) : term V → tp → Prop :=
| T_var : ∀ x,
    T[ Γ ⊢ val_var x :→ Γ x ]
| T_lam : ∀ t σ τ,
    T[ Γ ,+ σ ⊢ t :→ τ ] →
    T[ Γ ⊢ val_lam t :→ tp_arrow σ τ ]
| T_app : ∀ t s σ τ,
    T[ Γ ⊢ t :→ tp_arrow σ τ ] →
    T[ Γ ⊢ s :→ σ ] →
    T[ Γ ⊢ tm_app t s :→ τ ]
where "T[ Γ ⊢ t :→ τ ]" := (@typing _ Γ t τ).

Lemma progress_aux {V : Set} Γ (t : term V) τ : T[ Γ ⊢ t :→ τ ] →
  V = Empty_set →
  (∃ v : value V, t = v) ∨ (∃ t', red t t').
Proof.
induction 1 as [ A Γ x | A Γ t σ τ Ht IHt | A Γ t s σ τ Ht IHt Hs IHs ];
  intro HV.
+ eauto.
+ eauto.
+ right.
  destruct (IHt HV) as [ [ v Heq ] | [ t' Hred ] ].
  - rewrite_all Heq; clear Heq.
    destruct (IHs HV) as [ [ w Heq ] | [ s' Hred ] ].
    * subst.
      destruct v as [ [] | ].
      eexists; apply red_beta.
    * eexists; apply red_app2; eassumption.
  - eexists; apply red_app1; eassumption.
Qed.

Lemma progress (t : term0) τ : T[ ∅ ⊢ t :→ τ ] →
  (∃ v : value0, t = v) ∨ (∃ t', red t t').
Proof.
intro H; eapply progress_aux; eauto.
Qed.

Lemma typing_map {A B : Set}
  (Γ : env A) (Δ : env B)
  (f : A → B) t τ :
  (∀ x, Δ (f x) = Γ x) →
  T[ Γ ⊢ t :→ τ ] →
  T[ Δ ⊢ tmap f t :→ τ ].
Proof.
intros Hf Htp.
generalize B Δ f Hf; clear B Δ f Hf.
induction Htp as [ A Γ x | A Γ t σ τ Ht IHt | A Γ t s σ τ Ht IHt Hs IHs ];
  intros B Δ f Hf; simpl.
+ rewrite <- Hf; constructor.
+ constructor; apply IHt.
  intros [ | x ]; simpl; auto.
+ econstructor; eauto.
Qed.

Lemma typing_bind {A B : Set}
  (Γ : env A) (Δ : env B)
  (f : A → value B) t τ :
  (∀ x, T[ Δ ⊢ f x :→ Γ x ]) →
  T[ Γ ⊢ t :→ τ ] →
  T[ Δ ⊢ tbind f t :→ τ ].
Proof.
intros Hf Htp.
generalize B Δ f Hf; clear B Δ f Hf.
induction Htp as [ A Γ x | A Γ t σ τ Ht IHt | A Γ t s σ τ Ht IHt Hs IHs ];
  intros B Δ f Hf; simpl.
+ apply Hf.
+ constructor. apply IHt.
  destruct x as [ | x ]; simpl; [ constructor | ].
  eapply (typing_map _ _ _ (f x)); [ | apply Hf ].
  reflexivity.
+ econstructor; eauto.
Qed.

Theorem subject_reduction (t t' : term0) τ :
  red t t' → T[ ∅ ⊢ t :→ τ ] → T[ ∅ ⊢ t' :→ τ ].
Proof.
intro Hred; generalize τ; clear τ.
induction Hred as [ t v | t t' s Hred IH | v t t' Hred IH ]; intro τ.
+ inversion_clear 1.
  match goal with
  | [ H: T[ _ ⊢ tm_value (val_lam _) :→ _ ] |- _ ] => inversion_clear H
  end.
  eapply typing_bind; [ | eassumption ].
  destruct x as [ | [] ]; assumption.
+ inversion_clear 1.
  econstructor; [ | eassumption ]; auto.
+ inversion_clear 1.
  econstructor; [ eassumption | ]; auto.
Qed.

Lemma red_beta' {V : Set} (t : term (inc V))
  (v : value V) (t' : term V) :
  t' = tsubst t v →
  red (tm_app (val_lam t) v) t'.
Proof.
intro; subst; constructor.
Qed.

Lemma red_bind {A B : Set} (f : A → value B) t t' :
  red t t' →
  red (tbind f t) (tbind f t').
Proof.
induction 1 as [ t v | t t' s Hred IH | v t t' Hred IH ]; simpl.
+ apply red_beta'.
  rewrite tmonad_bind_bind'.
  symmetry; apply tmonad_bind_bind.
  intros [ | x ]; simpl; [ reflexivity | ].
  erewrite vmonad_bind_map; simpl.
  - rewrite vmonad_map_id'; apply vmonad_bind_return'.
  - reflexivity.
+ apply red_app1; assumption.
+ apply red_app2; assumption.
Qed.